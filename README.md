# Kubernetes single-master cluster on Ubuntu Cloud
## Contents:
* Example playbook and inventory files.
* Task to fetch credentials to the ansible runner machine.
* Manifests to deploy the dashboard, a load balancer and an ingress.


## How to use:
* The use of [cloud-init][cloud-init] is suggested, create an **ansible user** with a **ssh key** on every machine.
* Use your hypervisor of choice or maybe the cloud and deploy at least **1 kube_master** and **1 kube_node**.
* Populate your ansible [inventory][hosts-ini] with hostnames and IPs of the machines (example below).
```
[all]
kube-master-001 ansible_host=10.1.0.10
kube-node-001   ansible_host=10.1.0.11
kube-node-002   ansible_host=10.1.0.12

[kube_master]
kube-master-001

[kube_node]
kube-node-001
kube-node-002
(...)
```

### Choose your install option, either:
* Install barebones:
  ```bash
  ansible-playbook site.yml -K
  ```

* Install fully featured:
  ```bash
  ansible-playbook site.yml -K --tags 'bootstrap,master,nodes,metallb,ingress,dashboard,tokens'
  ```
  (requires changes on [`ansible/group_vars/kube_master.yml`][kube_master.yml] for [MetalLB][metallb-l2])


## How to upgrade:
WIP: planning to test [`kubeadm upgrade`][kubeadm-upgrade-docs].


## ! WARNING !
This is intended for a testing/development cluster, there are no verifications of any sort.  
This deployment should be fine for a GitLab Runner.  
Security might be overlooked for convenience.  


## Resources:
Extended documentation ([docs/extras.md][docs-extras]).  

This is based on the official [kubeadm documentation][kubeadm-docs].  
The [Ubuntu Cloud 18.04 LTS][ubuntu-cloud] image was used.


[cloud-init]: https://cloudinit.readthedocs.io/en/latest/
[docs-extras]: docs/extras.md
[hosts-ini]: ansible/hosts.ini
[kube_master.yml]: ansible/group_vars/kube_master.yml
[kubeadm-docs]: https://kubernetes.io/docs/reference/setup-tools/kubeadm/
[kubeadm-upgrade-docs]: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-upgrade/
[metallb-l2]: https://metallb.universe.tf/configuration/#layer-2-configuration
[ubuntu-cloud]: https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

