# Extras:
## To deploy and/or use the [dashboard][dashboard-docs] and `kubectl` on your machine:
```bash
ansible-playbook site.yml -K --tags 'dashboard,tokens'
```

* There should be a `tokens` directory with two files (`config` and `dashboard-token.txt`).
* Copy the `config` file into the `~/.kube` directory on your local machine.
* Run on your local machine:  
  `kubectl port-forward -n kubernetes-dashboard svc/kubernetes-dashboard 8443:443`
* The dashboard will be available at [> this link <][dashboard-api], open it on a new browser tab.
* Select `Token` and paste the contents from `dashboard-token.txt` on `Enter token`.
* Finally click `SIGN IN`.


## To deploy single features, use the ansible tags, such as:
* `dashboard`
* `tokens`
* `metallb`
* `ingress` (requires MetalLB)

```bash
ansible-playbook site.yml -K --tags 'tag1,tag2'
```

## To just install packages and OS tweaks on machines:
```bash
ansible-playbook boostrap.yml -K --tags 'bootstrap'
```

## Reset cluster (without bootstrapping again):
```bash
ansible-playbook site.yml -K --tags 'master,nodes' --extra-vars "reset_cluster=true"
```

## To add a new `kube-node-`, run:
```bash
ansible-playbook boostrap.yml -K --tags 'bootstrap-node' --limit 'kube-node-003, kube-node-004'
ansible-playbook site.yml -K --tags 'nodes'
```

# Resources:
Kubernetes [dashboard documentation][dashboard-docs].  

[dashboard-api]: https://127.0.0.1:8443
[dashboard-docs]: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

